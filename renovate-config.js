module.exports = {
  "automerge": true,
  "extends": ["config:recommended", ":rebaseStalePrs"],
  "dependencyDashboard": false,
  "rebaseWhen": "conflicted",
  "suppressNotifications": ["prIgnoreNotification"],
  "username": "szymonrichert.pl bot",
  "gitAuthor": "szymonrichert.pl bot <bot@szymonrichert.pl>",
  "allowedCommands": [".*"],
  "allowCommandTemplating": true,
  "platform": "gitlab",
  "endpoint": process.env.CI_API_V4_URL,
  "token": process.env.BOT_TOKEN,
  "onboarding": false,
  "onboardingConfig": {"extends": ["config:recommended"]},
  "repositories": [process.env.CI_PROJECT_PATH]
};

