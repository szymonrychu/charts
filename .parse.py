#!/usr/bin/env python3

import os

import jinja2

def main():
    _template_path = os.environ.get('TEMPLATE_PATH', '.sub.gitlab-ci.yml.j2')
    _output_path = os.environ.get('OUTPUT_PATH', '.sub.gitlab-ci.yml')
    _action = os.environ.get('ACTION', 'apply')
    charts_list = []
    for root, dirs, files in os.walk('./'):
        for file in files: 
            if 'Chart.yaml' == file:
                chart_yaml_path = os.path.join(root, file)[2:]
                chart_root = os.path.dirname(chart_yaml_path)
                name = os.path.basename(chart_root)
                charts_list.append({
                    'name': name, 
                    'root_path': chart_root
                })
    
    templateLoader = jinja2.FileSystemLoader(searchpath="./")
    templateEnv = jinja2.Environment(loader=templateLoader)
    template = templateEnv.get_template(_template_path)
    output_text = template.render(charts=charts_list, action=_action)  # this is where to put args to the template renderer

    with open(_output_path, 'w') as f:
        f.write(output_text)


if __name__ == '__main__':
    main()
